import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './ui/home/app.component';
import { ListBooksComponent } from './ui/list-books/list-books.component';
import { RegisterBookComponent } from './ui/register-book/register-book.component';
import { DetailsBookComponent } from './ui/details-book/details-book.component';
import { SearchBarComponent } from './ui/search-bar/search-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppMaterialModule } from "./app-material.module";

@NgModule({
  declarations: [
    AppComponent,
    ListBooksComponent,
    RegisterBookComponent,
    DetailsBookComponent,
    SearchBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppMaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
