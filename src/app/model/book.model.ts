export class Book {
    public id: number
    public title: string
    public author: string
    public edition: number
    public publicationYear: number
    public summary: string
    public publisher: string
    public pages: number
    public isbn: string
}

export class RegisterBook {
    public title: string
    public authorId: number
    public edition: number
    public publicationYear: number
    public summary: string
    public publisherId: number
    public pages: number
    public isbn: string

    constructor(title: string,
                authorId: number,
                edition: number,
                publicationYear: number,
                summary: string,
                publisherId: number,
                pages: number,
                isbn: string) {
        
        this.title = title
        this.authorId = authorId
        this.edition = edition
        this.publicationYear = publicationYear
        this.summary = summary
        this.publisherId = publisherId
        this.pages = pages
        this.isbn = isbn
    }
}