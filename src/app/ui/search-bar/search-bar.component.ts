import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { SearchFilter } from "../../model/search.model";

@Component({
    selector: 'app-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent {

    @Output() onSearch = new EventEmitter<SearchFilter>()
    @Output() onClear = new EventEmitter()

    formSearch = this.formBuilder.group({
        searchType: [1, [Validators.required]],
        searchText: ['', [Validators.required]]
    })

    constructor(private formBuilder: FormBuilder) { }

    search() {
        let filter = new SearchFilter()
        filter.type = this.searchType.value
        filter.text = this.searchText.value

        this.onSearch.emit(filter)
    }

    clearSearch() {
        this.searchText.setValue("")
        this.searchText.setErrors(null)
        this.searchType.setValue(1)
        
        this.onClear.emit()
    }

    get searchType() {
        return this.formSearch.controls.searchType
    }

    get searchText() {
        return this.formSearch.controls.searchText
    }
}
