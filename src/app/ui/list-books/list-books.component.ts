import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Book } from 'src/app/model/book.model';
import { SearchFilter } from 'src/app/model/search.model';
import { BookService } from 'src/app/service/book.service';
import { DetailsBookComponent } from '../details-book/details-book.component';

@Component({
    selector: 'app-list-books',
    templateUrl: './list-books.component.html',
    styleUrls: ['./list-books.component.css'],
})
export class ListBooksComponent implements OnInit {

    books: Book[] = []
    colsToDisplay = ['title', 'author', 'publisher', 'edition', 'isbn', 'year']

    constructor(private bookService: BookService,
                private dialog: MatDialog) { }

    ngOnInit() {
        this.findBooks()
    }

    findBooks() {
        this.bookService.findAll().subscribe(
            response => this.onSearchResult(response)
        )
    }

    searchBook(filter: SearchFilter) {
        this.bookService.find(filter).subscribe(
            response => this.onSearchResult(response)
        )
    }

    onSearchResult(result: Book[]) {
        if (result != null) {
            this.books = result
        }
    }    

    showDetails(id: number) {
        this.dialog.open(DetailsBookComponent, {
            width: '600px',
            data: {
                bookId: id
            }
        })
    }
}