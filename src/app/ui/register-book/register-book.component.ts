import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Author } from 'src/app/model/author.model';
import { RegisterBook } from 'src/app/model/book.model';
import { Publisher } from 'src/app/model/publisher.model';
import { AuthorService } from 'src/app/service/author.service';
import { BookService } from 'src/app/service/book.service';
import { PublisherService } from 'src/app/service/publisher.service';

@Component({
    selector: 'app-register-book',
    templateUrl: './register-book.component.html',
    styleUrls: ['./register-book.component.css']
})
export class RegisterBookComponent implements OnInit {

    formBook = this.formBuilder.group({
        title: ['', [Validators.required]],
        author: [null, [Validators.required]],
        publisher: [null, [Validators.required]],
        summary: ['', [Validators.required]],
        edition: ['', [Validators.required]],
        isbn: ['', [Validators.required]],
        publicationYear: ['', [Validators.required]],
        pages: ['', [Validators.required]]
    })

    authors: Author[] = []
    publishers: Publisher[] = []

    constructor(private formBuilder: FormBuilder,
        private router: Router,
        private bookService: BookService,
        private authorService: AuthorService,
        private publisherService: PublisherService) { }

    ngOnInit() {
        this.findAuthors()
        this.findPublishers()
    }

    findAuthors() {
        this.authorService.findAll().subscribe( result =>
            this.authors = result            
        )
    }

    findPublishers() {
        this.publisherService.findAll().subscribe(result => 
            this.publishers = result
        )
    }

    save() {
        let book = new RegisterBook(
            this.title.value,
            this.author.value,
            this.edition.value,
            this.publicationYear.value,
            this.summary.value,
            this.publisher.value,
            this.pages.value,
            this.isbn.value
        )

        this.bookService.save(book).subscribe(
            result => this.cancel()
        )
    }

    cancel() {
        this.router.navigateByUrl("/")
    }

    get title() {
        return this.formBook.controls.title
    }

    get author() {
        return this.formBook.controls.author
    }

    get publisher() {
        return this.formBook.controls.publisher
    }

    get summary() {
        return this.formBook.controls.summary
    }

    get isbn() {
        return this.formBook.controls.isbn
    }

    get edition() {
        return this.formBook.controls.edition
    }

    get publicationYear() {
        return this.formBook.controls.publicationYear
    }

    get pages() {
        return this.formBook.controls.pages
    }
}
