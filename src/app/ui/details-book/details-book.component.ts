import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Book } from 'src/app/model/book.model';
import { BookService } from 'src/app/service/book.service';

@Component({
    selector: 'app-details-book',
    templateUrl: './details-book.component.html',
    styleUrls: ['./details-book.component.css']
})
export class DetailsBookComponent implements OnInit {

    book: Book = new Book()

    constructor(private service: BookService,
                @Inject(MAT_DIALOG_DATA) public params: { bookId: number }) { }

    ngOnInit(): void {
        this.findBook(this.params.bookId)
    }

    findBook(bookId: number) {
        this.service.findById(bookId).subscribe(
            result => {
                this.book = result
            }
        )
    }
}
