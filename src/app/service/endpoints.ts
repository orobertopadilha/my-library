const URL_BASE = "http://localhost:8080"

export const WS_BOOKS = URL_BASE + "/book/"
export const WS_FIND_BOOK_BY_TITLE = WS_BOOKS + "byTitle?title="
export const WS_FIND_BOOK_BY_ISBN = WS_BOOKS + "byIsbn/"

export const WS_AUTHORS = URL_BASE + "/author"
export const WS_PUBLISHERS = URL_BASE + "/publisher"