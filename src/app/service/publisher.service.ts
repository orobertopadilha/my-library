import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Author } from "../model/author.model";
import { Publisher } from "../model/publisher.model";
import { DefaultResponse } from "../model/response.model";
import { WS_PUBLISHERS } from "./endpoints";

@Injectable({ providedIn: 'root' })
export class PublisherService {

    constructor(private http: HttpClient) {
    }

    findAll(): Observable<Publisher[]> {
        return this.http.get<DefaultResponse<Author[]>>(WS_PUBLISHERS).pipe(
            map(response => response.result)
        )
    }
}