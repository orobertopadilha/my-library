import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, pipe } from "rxjs";
import { map } from "rxjs/operators"
import { Book, RegisterBook } from "../model/book.model";
import { DefaultResponse } from "../model/response.model";
import { SearchFilter } from "../model/search.model";
import { WS_BOOKS, WS_FIND_BOOK_BY_ISBN, WS_FIND_BOOK_BY_TITLE } from "./endpoints";

@Injectable({
    providedIn: 'root'
})
export class BookService {

    constructor(private http: HttpClient) {

    }

    findAll(): Observable<Book[]> {
        return this.http.get<DefaultResponse<Book[]>>(WS_BOOKS).pipe(
            map(response => response.result)
        )
    }

    find(filter: SearchFilter): Observable<Book[]> {
        if (filter.type == 1) {
            return this.findByTitle(filter.text)
        } else {
            return this.findByIsbn(filter.text)
        }
    }

    findByTitle(title: string): Observable<Book[]> {
        return this.http.get<DefaultResponse<Book[]>>(WS_FIND_BOOK_BY_TITLE + title).pipe(
            map(response => response.result)
        )
    }

    findByIsbn(isbn: string): Observable<Book[]> {
        return this.http.get<DefaultResponse<Book>>(WS_FIND_BOOK_BY_ISBN + isbn).pipe(
            map(response => {
                let list = new Array<Book>()
                list.push(response.result)
                return list
            })
        )       
    }

    save(book: RegisterBook): Observable<boolean> {
        return this.http.post<DefaultResponse<boolean>>(WS_BOOKS, book).pipe(
            map(response => response.result)
        )
    }

    findById(id: number): Observable<Book> {
        return this.http.get<DefaultResponse<Book>>(WS_BOOKS + id).pipe(
            map(response => response.result)
        )
    }

}