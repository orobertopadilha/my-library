import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Author } from "../model/author.model";
import { DefaultResponse } from "../model/response.model";
import { WS_AUTHORS } from "./endpoints";

@Injectable({ providedIn: 'root' })
export class AuthorService {

    constructor(private http: HttpClient) {
    }

    findAll(): Observable<Author[]> {
        return this.http.get<DefaultResponse<Author[]>>(WS_AUTHORS).pipe(
            map(response => response.result)
        )
    }
}