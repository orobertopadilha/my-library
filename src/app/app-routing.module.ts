import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsBookComponent } from './ui/details-book/details-book.component';
import { ListBooksComponent } from './ui/list-books/list-books.component';
import { RegisterBookComponent } from './ui/register-book/register-book.component';

const routes: Routes = [
    { path: 'list-books', component: ListBooksComponent },
    { path: 'register-book', component: RegisterBookComponent },
    { path: '', pathMatch: 'full', redirectTo: '/list-books'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
